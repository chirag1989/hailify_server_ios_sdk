Pod::Spec.new do |s|
  s.name         = "HailifyFleetSdk-iOS"
  s.version      = "1.0.5"
  s.summary      = "HailifyFleetSdk SDK 1.0.5"
  s.homepage     = "https://www.drivehailify.com/"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "Hailify" => "support@drivehailify.com" }
  s.source       = { :git => "https://chirag_drivehailify@bitbucket.org/chirag1989/hailify_server_ios_sdk.git", :tag => s.version }
  s.platform     = :ios, '11.0'
  s.requires_arc = true
  s.swift_version = '5'
  s.xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => '$(inherited)', 'CLANG_ENABLE_MODULES' => 'YES', 'ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES' => 'YES' }
  # Using subspecs to support installation without Localization part
  s.default_subspecs = 'UI'
  s.pod_target_xcconfig = {
    'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64',
    'BUILD_LIBRARY_FOR_DISTRIBUTION' => 'YES'
  }
  s.user_target_xcconfig = { 
    'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64',
    'BUILD_LIBRARY_FOR_DISTRIBUTION' => 'YES'
  }

  s.subspec 'UI' do |ss|
    ss.platform     = :ios, '11.0'
    ss.ios.vendored_frameworks = "Hailify_iOS_SDK/1.0.5/HailifyFleetSDK.framework"
    ss.preserve_paths = "Hailify_iOS_SDK/1.0.5/*.framework"
  end


  s.dependency "Alamofire", '5.5.0'
  s.dependency "EVReflection", '5.10.1'
  s.dependency "HGCircularSlider", '2.2.0'
  s.dependency "CameraManager", '5.1.3'
  s.dependency "SwiftSignalRClient", '0.9.0'


end
